call plug#begin(stdpath('data') . '/plugged')
  Plug 'mattn/emmet-vim'
  Plug 'morhetz/gruvbox'
  Plug 'vim-ruby/vim-ruby'
  Plug 'hallison/vim-rdoc'
  Plug 'vim-scripts/paredit.vim'
  Plug 'ccorn90/vim-tsv'
  Plug 'https://codeberg.org/ngn/k.git', {'rtp': 'vim-k'}
  Plug 'https://gitlab.com/HiPhish/guile.vim'
call plug#end()

set tabstop=2
set expandtab
set shiftwidth=2
set colorcolumn=80
set textwidth=80
colorscheme gruvbox
