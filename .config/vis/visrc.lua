-- load standard vis module, providing parts of the Lua API
require('vis')

local pk = require'vis-parkour'

vis.events.subscribe(vis.events.INIT, function()
  -- Your global configuration options
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
  -- Your per window configuration options e.g.
  -- vis:command('set number')
  vis:command 'set tabwidth 2'
  vis:command 'set expandtab on'
  vis:command 'set autoindent on'
end)
